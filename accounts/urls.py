from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect
from accounts.views import *

urlpatterns = [
    path("login/", user_login, name='login'),
    path("logout/", user_logout, name="logout"),
    path("signup/", user_signup, name='signup')
]
