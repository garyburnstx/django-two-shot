from django.forms import ModelForm
from django import forms
from django.contrib.auth.models import User

class LoginForm(forms.Form):
    # class Meta:
        username = forms.CharField(max_length=150)
        password = forms.CharField(widget=forms.PasswordInput, max_length=150)

class SignupForm(forms.Form):
        username = forms.CharField(max_length=150)
        password = forms.CharField(widget=forms.PasswordInput, max_length=150)
        password_confirmation = forms.CharField(widget=forms.PasswordInput, max_length=150)
